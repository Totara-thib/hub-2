# Semantic Release

This project uses the project [semantic-release](https://github.com/semantic-release/semantic-release)
to generate release and changelogs in the CI. 

See more about how we handle `semantic-release` in the 
[handbook](https://handbook.go2scale.io/development-gitflow/#conventional-commits)
